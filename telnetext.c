
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>


int Connect(const char * host, int port)
{
    int res =-1;
    do{
        int     sockfd;
        struct sockaddr_in servaddr;

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if(-1==sockfd)
            break;
            
        bzero(&servaddr, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(port);
        int rt =inet_pton(AF_INET, host, &servaddr.sin_addr);
        if(1!=rt)
        {
            printf("host{%s} error! rt=%d.\n",host,rt);
            break;
        }
        
        if(-1==connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)))
        {
            printf("connect{%s:%d} error! \n",host,port);
            break;
        }
        
        res =sockfd;
    }while(0);
    return res;
}

int main(int argc, char * argv[])
{
    if(argc<4)
    {
        printf("Usage: %s <host> <port> <data> \n",argv[0]);
        return 1;
    }
    
    int sockfd =Connect(argv[1], atoi(argv[2]));
    if(0!=sockfd)
    {
        return 1;
    }
    
    const char * buf =argv[3];
    size_t len =strlen(buf);
    ssize_t ns =send(sockfd,buf,len,0);
    printf("Sent %lld bytes, %s", (long long)ns,buf);
    
    close(sockfd);
    return 0;
}
